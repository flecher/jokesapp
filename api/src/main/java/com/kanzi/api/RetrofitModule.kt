package com.kanzi.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RetrofitModule {

    @Provides
    @Singleton
    internal fun provideOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(addUserAgent())
            .addInterceptor(
                HttpLoggingInterceptor().apply {
                    level =
                        HttpLoggingInterceptor.Level.BODY
                }
            )
            .build()

    @Provides
    @Singleton
    internal fun provideRetrofit(client: OkHttpClient, gson: Gson): JokesApi =
        Retrofit.Builder()
            .baseUrl("https://localhost/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(JokesApi::class.java)

    @Provides
    @Singleton
    internal fun provideGson(): Gson =
        GsonBuilder()
            .setLenient()
            .create()

    private fun addUserAgent(): (Interceptor.Chain) -> Response {
        return {
            val userAgent = it.request().newBuilder()
                .addHeader(
                    "User-Agent",
                    "Surfshark android test task https://bitbucket.org/flecher/jokesapp/src/master/"
                )
                .build()
            it.proceed(userAgent)
        }
    }
}
