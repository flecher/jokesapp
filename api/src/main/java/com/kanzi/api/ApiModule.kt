package com.kanzi.api

import com.kanzi.domain.RemoteApi
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
internal interface ApiModule {
    @Binds
    fun bindRemoteApi(service: JokesApiService): RemoteApi
}
