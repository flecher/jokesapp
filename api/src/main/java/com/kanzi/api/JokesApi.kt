package com.kanzi.api

import com.kanzi.api.model.ChuckNorrisFact
import com.kanzi.api.model.DadJoke
import retrofit2.http.GET
import retrofit2.http.Headers

private const val CONTENT_TYPE_JSON = "Accept: application/json"

internal interface JokesApi {

    @GET("https://icanhazdadjoke.com/")
    @Headers(CONTENT_TYPE_JSON)
    suspend fun getDadJoke(): DadJoke

    @GET("https://api.chucknorris.io/jokes/random")
    suspend fun getChuckNorrisFact(): ChuckNorrisFact
}
