package com.kanzi.api.model

internal data class DadJoke(
    val id: String,
    val joke: String,
    val status: Int
)
