package com.kanzi.api.model

import com.google.gson.annotations.SerializedName

internal data class ChuckNorrisFact(
    @SerializedName("icon_url")
    val iconUrl: String,
    val id: String,
    val url: String,
    @SerializedName("value")
    val joke: String
)
