package com.kanzi.api

import com.kanzi.domain.RemoteApi
import javax.inject.Inject

internal class JokesApiService @Inject constructor(
    private val api: JokesApi
) : RemoteApi {

    override suspend fun getDadJoke(): String {
        return api.getDadJoke().joke
    }

    override suspend fun getChuckNorrisFact(): String {
        return api.getChuckNorrisFact().joke
    }
}
