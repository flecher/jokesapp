package com.kanzi.jokesapp.ui.joke

import androidx.lifecycle.SavedStateHandle
import com.kanzi.domain.models.JokeType
import com.kanzi.domain.usecase.GetJokeUseCase
import com.kanzi.jokesapp.utils.CoroutinesTestRule
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.runner.Result

@OptIn(ExperimentalCoroutinesApi::class)
class JokeViewModelTest {

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    private val savedStateHandle = mockk<SavedStateHandle>()
    private val getJokeUseCase = mockk<GetJokeUseCase>()

    private lateinit var viewModel: JokeViewModel

    @Nested
    inner class DadJokes {

        private val jokeType = JokeType.Dad
        private val joke = " joke "

        @BeforeEach
        fun setUp() {
            coEvery { getJokeUseCase(jokeType) } coAnswers {
                com.kanzi.domain.utils.Result.Success(joke)
            }
            every { savedStateHandle.get<Any>(any()) } returns jokeType
            viewModel = JokeViewModel(savedStateHandle, getJokeUseCase)
        }

        @Nested
        inner class GetJokeSuccess {

            @Test
            fun `received joke`() {
                coroutinesTestRule.testDispatcher.runBlockingTest {
                    coEvery { getJokeUseCase(jokeType) } coAnswers {
                        com.kanzi.domain.utils.Result.Success(joke)
                    }
                    viewModel.tappedGetJoke()
                    coVerify { getJokeUseCase(jokeType) }
                    assertEquals(joke, viewModel.state.value.joke)
                    assertFalse(viewModel.state.value.isLoading)
                }
            }

            @Test
            fun `received failure`() {
                coroutinesTestRule.testDispatcher.runBlockingTest {
                    coEvery { getJokeUseCase(jokeType) } coAnswers {
                        com.kanzi.domain.utils.Result.GenericError()
                    }
                    viewModel.tappedGetJoke()
                    coVerify { getJokeUseCase(jokeType) }
                    assertNotNull(viewModel.state.value.showError)
                    assertFalse(viewModel.state.value.isLoading)
                }
            }
        }
    }

    @Nested
    inner class ChuckJokes {

        private val jokeType = JokeType.ChuckNorris
        private val joke = " joke "

        @BeforeEach
        fun setUp() {
            coEvery { getJokeUseCase(jokeType) } coAnswers {
                com.kanzi.domain.utils.Result.Success(joke)
            }
            every { savedStateHandle.get<Any>(any()) } returns jokeType
            viewModel = JokeViewModel(savedStateHandle, getJokeUseCase)
        }

        @Nested
        inner class GetJokeSuccess {

            @Test
            fun `received joke`() {
                coroutinesTestRule.testDispatcher.runBlockingTest {
                    viewModel.tappedGetJoke()
                    coVerify { getJokeUseCase(jokeType) }
                    assertEquals(joke, viewModel.state.value.joke)
                    assertFalse(viewModel.state.value.isLoading)
                }
            }

            @Test
            fun `received failure`() {
                coroutinesTestRule.testDispatcher.runBlockingTest {
                    coEvery { getJokeUseCase(jokeType) } coAnswers {
                        com.kanzi.domain.utils.Result.GenericError()
                    }
                    viewModel.tappedGetJoke()
                    coVerify { getJokeUseCase(jokeType) }
                    assertNotNull(viewModel.state.value.showError)
                    assertFalse(viewModel.state.value.isLoading)
                }
            }
        }
    }
}
