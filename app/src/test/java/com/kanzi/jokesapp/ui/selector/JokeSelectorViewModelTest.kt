package com.kanzi.jokesapp.ui.selector

import com.kanzi.domain.models.JokeType
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class JokeSelectorViewModelTest {

    private lateinit var viewModel: JokeSelectorViewModel

    @BeforeEach
    fun setUp() {
        viewModel = JokeSelectorViewModel()
    }

    @Nested
    inner class TappedDadJokes {

        @BeforeEach
        fun setUp() {
            viewModel.tappedDadJokes()
        }

        @Test
        fun dadJokesEventIsPresent() {
            assertEquals(JokeType.Dad, viewModel.state.value.showJoke?.peekContent())
        }
    }

    @Nested
    inner class TappedChuckFacts {

        @BeforeEach
        fun setUp() {
            viewModel.tappedChuckNorrisFacts()
        }

        @Test
        fun dadJokesEventIsPresent() {
            assertEquals(JokeType.ChuckNorris, viewModel.state.value.showJoke?.peekContent())
        }
    }
}
