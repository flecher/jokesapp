package com.kanzi.jokesapp.utils.ext

import androidx.lifecycle.SavedStateHandle

inline fun <reified T : Any> SavedStateHandle.getValue(key: String, default: T? = null) =
    lazy {
        val value = get<T>(key)
        requireNotNull(if (value is T) value else default) { key }
    }
