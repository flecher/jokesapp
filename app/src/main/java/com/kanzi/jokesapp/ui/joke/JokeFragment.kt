package com.kanzi.jokesapp.ui.joke

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.kanzi.domain.models.JokeType
import com.kanzi.jokesapp.R
import com.kanzi.jokesapp.databinding.FragmentJokeBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class JokeFragment : Fragment(R.layout.fragment_joke) {

    private val viewModel: JokeViewModel by viewModels()

    private lateinit var binding: FragmentJokeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentJokeBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.jokeGetButton.setOnClickListener {
            viewModel.tappedGetJoke()
        }
        observeState()
    }

    private fun observeState() {
        lifecycleScope.launchWhenCreated {
            viewModel.state.collect { state ->
                with(binding) {
                    jokeGetButton.text = when (state.jokeType) {
                        JokeType.Dad -> {
                            getString(R.string.get_dad_joke)
                        }
                        JokeType.ChuckNorris -> {
                            getString(R.string.get_chuck_norris_fact)
                        }
                    }
                    jokeText.text = state.joke
                    jokeLoading.root.isVisible = state.isLoading
                }
                state.showError?.getContentIfNotHandled()?.let {
                    Snackbar.make(
                        binding.root,
                        getString(R.string.request_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            }
        }
    }
}
