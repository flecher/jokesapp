package com.kanzi.jokesapp.ui.selector

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.kanzi.domain.models.JokeType
import com.kanzi.jokesapp.R
import com.kanzi.jokesapp.databinding.FragmentJokeSelectorBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class JokeSelectorFragment : Fragment(R.layout.fragment_joke_selector) {

    private val viewModel: JokeSelectorViewModel by viewModels()

    private lateinit var binding: FragmentJokeSelectorBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentJokeSelectorBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            selectorChuckNorris.setOnClickListener {
                viewModel.tappedChuckNorrisFacts()
            }
            selectorDadJokes.setOnClickListener {
                viewModel.tappedDadJokes()
            }
        }
        observeState()
    }

    private fun observeState() {
        lifecycleScope.launchWhenCreated {
            viewModel.state.collect { state ->
                state.showJoke?.getContentIfNotHandled()?.let {
                    showJokes(it)
                }
            }
        }
    }

    private fun showJokes(jokeType: JokeType) {
        val action = JokeSelectorFragmentDirections.actionSelectorToJoke(jokeType)
        findNavController().navigate(action)
    }
}
