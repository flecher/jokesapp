package com.kanzi.jokesapp.ui.selector

import androidx.lifecycle.ViewModel
import com.kanzi.domain.models.JokeType
import com.kanzi.jokesapp.utils.ext.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

@HiltViewModel
class JokeSelectorViewModel @Inject constructor() : ViewModel() {

    private val _state = MutableStateFlow(ViewState())
    val state = _state.asStateFlow()

    fun tappedDadJokes() {
        onReduceEvent(Action.TappedDadJokes)
    }

    fun tappedChuckNorrisFacts() {
        onReduceEvent(Action.TappedChuckNorrisFacts)
    }

    private fun onReduceEvent(event: Action) {
        when (event) {
            Action.TappedChuckNorrisFacts -> {
                _state.value = ViewState(Event(JokeType.ChuckNorris))
            }
            Action.TappedDadJokes -> {
                _state.value = ViewState(Event(JokeType.Dad))
            }
        }
    }

    sealed class Action {
        object TappedDadJokes : Action()
        object TappedChuckNorrisFacts : Action()
    }

    data class ViewState(
        val showJoke: Event<JokeType>? = null
    )
}
