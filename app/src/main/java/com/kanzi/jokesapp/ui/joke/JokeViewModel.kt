package com.kanzi.jokesapp.ui.joke

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.kanzi.domain.models.JokeType
import com.kanzi.domain.usecase.GetJokeUseCase
import com.kanzi.domain.utils.Result
import com.kanzi.jokesapp.utils.ext.SimpleEvent
import com.kanzi.jokesapp.utils.ext.getValue
import com.kanzi.jokesapp.utils.ext.ioJob
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

private const val ARG_JOKE_TYPE = "jokeType"

@HiltViewModel
class JokeViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val getJokeUseCase: GetJokeUseCase
) : ViewModel() {

    private val jokeType by savedStateHandle.getValue<JokeType>(ARG_JOKE_TYPE)

    private val _state = MutableStateFlow(ViewState(jokeType))
    val state = _state.asStateFlow()

    init {
        getJoke()
    }

    fun tappedGetJoke() {
        getJoke()
    }

    private fun getJoke() {
        ioJob {
            _state.value = _state.value.copy(isLoading = true)
            when (val response = getJokeUseCase(jokeType)) {
                is Result.Success -> {
                    onReduceEvent(Action.ReceivedJoke(response.value))
                }
                else -> {
                    onReduceEvent(Action.ReceivedError)
                }
            }
        }
    }

    private fun onReduceEvent(event: Action) {
        when (event) {
            Action.ReceivedError -> {
                _state.value = _state.value.copy(
                    isLoading = false,
                    showError = SimpleEvent()
                )
            }
            is Action.ReceivedJoke -> {
                _state.value = _state.value.copy(
                    joke = event.joke,
                    isLoading = false
                )
            }
        }
    }

    sealed class Action {
        data class ReceivedJoke(val joke: String) : Action()
        object ReceivedError : Action()
    }

    data class ViewState(
        val jokeType: JokeType,
        val isLoading: Boolean = false,
        val joke: String = "",
        val showError: SimpleEvent? = null
    )
}
