package com.kanzi.domain.repository

import com.kanzi.domain.RemoteApi
import com.kanzi.domain.utils.Result
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

@OptIn(ExperimentalCoroutinesApi::class)
class GetJokeRepositoryImplTest {

    private val dispatcher = TestCoroutineDispatcher()

    private lateinit var getJokeRepositoryImpl: GetJokeRepositoryImpl
    private val api = mockk<RemoteApi>()

    @BeforeEach
    fun setUp() {
        getJokeRepositoryImpl = GetJokeRepositoryImpl(api, dispatcher)
    }

    @Nested
    inner class DadJoke {

        @Test
        fun `verify api is called`() {
            runBlockingTest {
                getJokeRepositoryImpl.getDadJoke()
                coVerify { api.getDadJoke() }
            }
        }

        @Nested
        inner class FetchSuccess {

            @Test
            fun `response is success`() {
                runBlockingTest {
                    coEvery { api.getDadJoke() } coAnswers { "Joke" }
                    val response = getJokeRepositoryImpl.getDadJoke()
                    assert(response is Result.Success)
                }
            }
        }

        @Nested
        inner class FetchFailure {

            @Test
            fun `response is failure`() {
                runBlockingTest {
                    val response = getJokeRepositoryImpl.getDadJoke()
                    assert(response is Result.GenericError)
                }
            }
        }
    }

    @Nested
    inner class ChuckJoke {

        @Test
        fun `verify api is called`() {
            runBlockingTest {
                getJokeRepositoryImpl.getChuckNorrisFact()
                coVerify { api.getChuckNorrisFact() }
            }
        }

        @Nested
        inner class FetchSuccess {

            @Test
            fun `response is success`() {
                runBlockingTest {
                    coEvery { api.getChuckNorrisFact() } coAnswers { "Joke" }
                    val response = getJokeRepositoryImpl.getChuckNorrisFact()
                    assert(response is Result.Success)
                }
            }
        }

        @Nested
        inner class FetchFailure {

            @Test
            fun `response is failure`() {
                runBlockingTest {
                    val response = getJokeRepositoryImpl.getChuckNorrisFact()
                    assert(response is Result.GenericError)
                }
            }
        }
    }
}
