package com.kanzi.domain.usecase

import com.kanzi.domain.models.JokeType
import com.kanzi.domain.repository.GetJokeRepositoryImpl
import com.kanzi.domain.utils.Result
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

@OptIn(ExperimentalCoroutinesApi::class)
internal class GetJokeUseCaseTest {

    private val jokeRepository = mockk<GetJokeRepositoryImpl>()

    private lateinit var useCase: GetJokeUseCase

    @BeforeEach
    fun setUp() {
        useCase = GetJokeUseCase(jokeRepository)
    }

    @Nested
    inner class DadJoke {

        private val jokeType = JokeType.Dad

        @Test
        fun `dad joke is called`() {
            runBlockingTest {
                coEvery { useCase(jokeType) } coAnswers { Result.Success("") }
                useCase(jokeType)
                coVerify { jokeRepository.getDadJoke() }
            }
        }

        @Nested
        inner class FetchSuccess {

            private val joke = "Some kind of joke"

            @Test
            fun `joke is received`() {
                runBlockingTest {
                    coEvery { useCase(jokeType) } coAnswers { Result.Success(joke) }
                    val response = useCase(jokeType)
                    assertEquals(response, Result.Success(joke))
                }
            }
        }

        @Nested
        inner class FetchFailed {

            @Test
            fun `error received`() {
                runBlockingTest {
                    coEvery { useCase(jokeType) } coAnswers { Result.NetworkError }
                    val response = useCase(jokeType)
                    assertEquals(response, Result.NetworkError)
                }
            }
        }
    }

    @Nested
    inner class ChuckFact {

        private val jokeType = JokeType.ChuckNorris

        @Test
        fun `chuck fact is called`() {
            runBlockingTest {
                coEvery { useCase(jokeType) } coAnswers { Result.Success("") }
                useCase(jokeType)
                coVerify { jokeRepository.getChuckNorrisFact() }
            }
        }

        @Nested
        inner class FetchSuccess {

            private val joke = "Some kind of joke"

            @Test
            fun `joke is received`() {
                runBlockingTest {
                    coEvery { useCase(jokeType) } coAnswers { Result.Success(joke) }
                    val response = useCase(jokeType)
                    assertEquals(response, Result.Success(joke))
                }
            }
        }

        @Nested
        inner class FetchFailed {

            @Test
            fun `error received`() {
                runBlockingTest {
                    coEvery { useCase(jokeType) } coAnswers { Result.NetworkError }
                    val response = useCase(jokeType)
                    assertEquals(response, Result.NetworkError)
                }
            }
        }
    }
}
