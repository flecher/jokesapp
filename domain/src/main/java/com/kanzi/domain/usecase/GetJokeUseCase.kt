package com.kanzi.domain.usecase

import com.kanzi.domain.models.JokeType
import com.kanzi.domain.repository.GetJokeRepository
import com.kanzi.domain.utils.Result
import javax.inject.Inject

class GetJokeUseCase @Inject constructor(
    private val getJokeRepository: GetJokeRepository
) {

    suspend operator fun invoke(jokeType: JokeType): Result<String> {
        return when (jokeType) {
            JokeType.ChuckNorris -> {
                getJokeRepository.getChuckNorrisFact()
            }
            JokeType.Dad -> {
                getJokeRepository.getDadJoke()
            }
        }
    }
}
