package com.kanzi.domain

interface RemoteApi {
    suspend fun getDadJoke(): String
    suspend fun getChuckNorrisFact(): String
}
