package com.kanzi.domain

import com.kanzi.domain.repository.GetJokeRepository
import com.kanzi.domain.repository.GetJokeRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object RepositoryModule {

    @Provides
    internal fun provideGetJokeRepository(getJokeRepositoryImpl: GetJokeRepositoryImpl): GetJokeRepository {
        return getJokeRepositoryImpl
    }
}
