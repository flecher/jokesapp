package com.kanzi.domain.repository

import com.kanzi.domain.utils.Result

interface GetJokeRepository {
    suspend fun getDadJoke(): Result<String>
    suspend fun getChuckNorrisFact(): Result<String>
}
