package com.kanzi.domain.repository

import com.kanzi.domain.RemoteApi
import com.kanzi.domain.common.Io
import com.kanzi.domain.utils.Result
import com.kanzi.domain.utils.safeApiCall
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class GetJokeRepositoryImpl @Inject constructor(
    private val api: RemoteApi,
    @Io private val dispatcher: CoroutineDispatcher
) : GetJokeRepository {

    override suspend fun getDadJoke(): Result<String> {
        return safeApiCall(dispatcher) { api.getDadJoke() }
    }

    override suspend fun getChuckNorrisFact(): Result<String> {
        return safeApiCall(dispatcher) { api.getChuckNorrisFact() }
    }
}
